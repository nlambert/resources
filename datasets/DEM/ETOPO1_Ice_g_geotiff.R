# Packages

library(sf)
library(raster)
library(sp)
library(tiff)

# dem 1

prj <- "+proj=robin +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs"
fact <- 200
dem <- raster("ETOPO1_Ice_g_geotiff.tif")

dem <- aggregate(dem, fact=fact, fun=mean)

dots <-  as(dem, 'SpatialPointsDataFrame')
dots <- st_as_sf(dots)

# dots$alwdgg[dots$alwdgg <0] <- 0
coords <- as.data.frame(st_coordinates(dots))
dots$X <- coords$X
dots$Y <- coords$Y
output <- dots %>% st_drop_geometry()
output <- output[,c("Y","X","ETOPO1_Ice_g_geotiff")]
colnames(output) <- c("lat","lon","elevation")
write.csv(output, 'ETOPO1.csv')

