# Packages

library(sf)
library(raster)
library(sp)
library(tiff)

# dem 1

prj <- "+proj=robin +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs"
fact <- 10
dem <- raster("alwdgg.tif")
dem <- aggregate(dem, fact=fact, fun=max)
dots <-  as(dem, 'SpatialPointsDataFrame')
dots <- st_as_sf(dots)
dim(dots)
dots$alwdgg[dots$alwdgg <0] <- 0
coords <- as.data.frame(st_coordinates(dots))
dots$X <- coords$X
dots$Y <- coords$Y
plot(st_geometry(dots))


output <- dots %>% st_drop_geometry()
output <- output[,c("Y","X","alwdgg")]
colnames(output) <- c("lat","lon","elevation")
output <- output[output$lat > -60,]
write.csv(output, 'dem.csv')

# dem 2
prj <- "+proj=robin +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs"
fact <- 10
dem <- raster("alwdgg.tif")
dem <- aggregate(dem, fact=fact, fun=max)
dots <-  as(dem, 'SpatialPointsDataFrame')
dots <- st_as_sf(dots)
dots$alwdgg[dots$alwdgg <0] <- 0
coords <- as.data.frame(st_coordinates(dots))
dots$X <- coords$X
dots$Y <- coords$Y
plot(st_geometry(dots))


output <- dots %>% st_drop_geometry()
output <- output[,c("Y","X","alwdgg")]
colnames(output) <- c("lat","lon","elevation")
output <- output[output$lat > -60,]
write.csv(output, 'dem2.csv')
