# library

library(sf)
library(raster)

r <- raster("gpw_v4_population_count_adjusted_to_2015_unwpp_country_totals_rev11_2020_30_min.tif")
land <- st_read("../land.json")

fact <- 1
dots <- aggregate(r, fact=fact, fun=sum)
dots <- as(dots, 'SpatialPointsDataFrame')
dots <- st_as_sf(dots)
colnames(dots) <- c("pop2020","geometry")

for (cellsize in 1:10){
grid <- st_make_grid(dots, cellsize = cellsize, square = FALSE)
grid <- aggregate(dots["pop2020"], grid, sum)
grid <- grid[grid$pop2020 > 0,]
result <- st_intersection(grid, land)
result <- result[,c("pop2020","geometry")]
st_write(paste0(result, "../output/pophex_",cellsize,"deg"))
}